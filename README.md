# SKIVE #

skive is a lightweight Bot engine to create your own Slack Bot.

### Try the library with a Console app ###
* Create a console application
* Install-Package Hunabku.Skive 
* Copy and paste the code below
```
#!c#

	class Program
	{
		static void Main(string[] args)
		{
			string authToken = args[0];

			var store = new EventHandlersStore()
				.Register("message", () => new EchoMessageHandler());

			using (var sb = new BotEngine(store))
			{
				sb.Connect(authToken).Wait();
				Console.ReadLine();
			}
		}
	}

```