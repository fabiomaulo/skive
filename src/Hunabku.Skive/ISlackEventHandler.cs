using System.Threading.Tasks;

namespace Hunabku.Skive
{
	public interface ISlackEventHandler
	{
		/// <summary>
		///   Handler for a specific slack event
		/// </summary>
		/// <param name="context">The context of the event shared by all hadlers handling the event.</param>
		Task Handle(ISlackEventContext context);
	}
}