﻿using System;
using System.Collections.Generic;

namespace Hunabku.Skive
{
	public class EventHandlersStore : ISlackEventHandlersStore
	{
		private readonly Dictionary<string, List<Func<ISlackEventHandler>>> handlersFactories =
			new Dictionary<string, List<Func<ISlackEventHandler>>>();

		public IEnumerable<ISlackEventHandler> GetHandlersOf(string eventType)
		{
			List<Func<ISlackEventHandler>> eventHandlersFactories;
			if (handlersFactories.TryGetValue(eventType, out eventHandlersFactories))
			{
				foreach (var func in eventHandlersFactories)
				{
					yield return func();
				}
			}
		}

		public bool HasHandlers(string eventType)
		{
			List<Func<ISlackEventHandler>> list;
			if (handlersFactories.TryGetValue(eventType, out list))
			{
				return list.Count > 0;
			}
			return false;
		}

		public EventHandlersStore Register(string eventType, Func<ISlackEventHandler> factory)
		{
			if (factory == null)
			{
				throw new ArgumentNullException(nameof(factory));
			}
			GetEventHandlersFactoriesFor(eventType).Add(factory);
			return this;
		}

		private List<Func<ISlackEventHandler>> GetEventHandlersFactoriesFor(string eventType)
		{
			List<Func<ISlackEventHandler>> list;
			if (!handlersFactories.TryGetValue(eventType, out list))
			{
				list = new List<Func<ISlackEventHandler>>();
				handlersFactories[eventType] = list;
			}
			return list;
		}
	}
}