﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Hunabku.Skive
{
	public static class SlackApi
	{
		public static Task<string> BotChatPostMessage(this ISlackEventContext context, string channelId, string text, string attachments= null)
		{
			var botProfile = context.BotProfile;
			var form = new Dictionary<string, string>
			{
				["token"] = context.AuthToken,
				["channel"] = channelId,
				["text"] = text,
				["as_user"] = "false",
				["icon_url"] = botProfile.Image48Url,
				["username"] = botProfile.Name
			};
			if (!string.IsNullOrWhiteSpace(attachments))
			{
				form["attachments"] = attachments;
			}

			return PostMethod(context, "chat.postMessage", form);
		}

		public static async Task<string> PostMethod(this ISlackEventContext context, string apiMethod,
			IEnumerable<KeyValuePair<string, string>> formContent)
		{
			using (var httpClient = HttpClient())
			{
				using (var response = await httpClient.PostAsync(apiMethod, new FormUrlEncodedContent(formContent)))
				{
					return await response.Content.ReadAsStringAsync();
				}
			}
		}

		public static async Task<string> GetMethod(this ISlackEventContext context, string apiMethod,
			IEnumerable<KeyValuePair<string, string>> queryVariables)
		{
			using (var httpClient = SlackApi.HttpClient())
			{
				string query;
				using (var content = new FormUrlEncodedContent(queryVariables))
				{
					query = await content.ReadAsStringAsync();
				}

				return await httpClient.GetStringAsync(string.Concat(apiMethod,"?",query));
			}
		}

		public static HttpClient HttpClient()
		{
			var httpClient = new HttpClient();
			httpClient.DefaultRequestHeaders.Accept.ParseAdd("application/json");
			httpClient.BaseAddress = new Uri("https://slack.com/api/");
			return httpClient;
		}

		public static bool UserIsBot(this ISlackEventContext context)
		{
			if (context == null || context.Event == null)
			{
				return false;
			}
			string userId = context.Event.user;
			return context.BotProfile?.Id.Equals(userId) ?? false;
		}

		public static ChannelType GetChannelType(this ISlackEventContext context)
		{
			if (context == null || context.Event == null)
			{
				return ChannelType.Undefined;
			}
			string channel = context.Event.channel;
			if (string.IsNullOrEmpty(channel))
			{
				return ChannelType.Undefined;
			}
			switch (channel[0])
			{
				case 'C':
					return ChannelType.Channel;
				case 'D':
					return ChannelType.DirectMessage;
				case 'G':
					return ChannelType.Group;
				default:
					return ChannelType.Undefined;
			}
		}
	}
}