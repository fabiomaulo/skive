﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Hunabku.Skive
{
	public class BotEngine : IDisposable
	{
		private readonly IEnvironmentBuilder environmentBuilder;
		private readonly ISlackEventHandlersStore handlersStore;
		private KeyValuePair<string, object>[] sharedEnvironment;

		public BotEngine(ISlackEventHandlersStore handlersStore)
			:this(handlersStore, null)
		{
		}

		public BotEngine(ISlackEventHandlersStore handlersStore, IEnvironmentBuilder environmentBuilder)
		{
			this.environmentBuilder = environmentBuilder;
			this.handlersStore = handlersStore ?? new NoOpStore();
		}

		public BotProfile Self { get; private set; }

		private string AuthToken { get; set; }

		private SocketListener Socket { get; set; }

		public void Dispose()
		{
			Socket?.Dispose();
			Socket = null;
		}

		public async Task Connect(string token)
		{
			var slackStart = await GetStartInfo(token);
			if (string.IsNullOrEmpty(slackStart))
			{
				throw new Exception("No way to connect to Slack.");
			}
			dynamic info = JsonConvert.DeserializeObject(slackStart);
			string okState = info.ok;
			if ("false".Equals(okState, StringComparison.OrdinalIgnoreCase))
			{
				throw new Exception("Something was wrong with Slack:" + slackStart);
			}
			Self = CreateBotProfile(info);
			AuthToken = token;
			IEnumerable<KeyValuePair<string, object>> environment = environmentBuilder?.BuildEnvironment(slackStart, info);
			sharedEnvironment = (environment ?? Enumerable.Empty<KeyValuePair<string, object>>()).ToArray();

			string webSocketUrl = info.url;
			Socket = new SocketListener(webSocketUrl)
				.OnMessage(async x =>
				{
					await HandleSlackEvent(x);
				});
			Socket.Connect();
		}

		private BotProfile CreateBotProfile(dynamic info)
		{
			var profile = new BotProfile();
			profile.Id = info.self.id;
			profile.Name = info.self.name;
			IEnumerable<dynamic> users = info.users;
			foreach (var user in users)
			{
				string uid = user.id;
				if (uid != profile.Id)
				{
					continue;
				}
				profile.Image48Url = user.profile?.image_48;
				break;
			}
			return profile;
		}

		private Task HandleSlackEvent(string serializedEventData)
		{
			dynamic eventData = JsonConvert.DeserializeObject(serializedEventData);
			string eventType = eventData.type;
			if (!handlersStore.HasHandlers(eventType))
			{
				return Task.FromResult((object) null);
			}
			SlackEventContext eventContext = CreateContext(serializedEventData, eventData);
			var handlers = handlersStore.GetHandlersOf(eventType).Select(x => x.Handle(eventContext)).ToArray();
			return Task.WhenAll(handlers);
		}

		private SlackEventContext CreateContext(string serializedEventData, dynamic eventData)
		{
			var eventContext = new SlackEventContext
			{
				AuthToken = AuthToken,
				BotProfile = Self,
				EventContent = serializedEventData,
				Event = eventData
			};
			eventContext.SetContext(sharedEnvironment);
			return eventContext;
		}

		private static async Task<string> GetStartInfo(string token)
		{
			using (var httpClient = SlackApi.HttpClient())
			{
				string query;
				using (var content = new FormUrlEncodedContent(new[]
				{
					new KeyValuePair<string, string>("token", token),
					new KeyValuePair<string, string>("simple_latest", "true"),
					new KeyValuePair<string, string>("no_unreads", "true")
				}))
				{
					query = await content.ReadAsStringAsync();
				}

				return await httpClient.GetStringAsync(string.Concat("rtm.start?",query));
			}
		}

		private class NoOpStore : ISlackEventHandlersStore
		{
			public IEnumerable<ISlackEventHandler> GetHandlersOf(string eventType)
			{
				yield break;
			}

			public bool HasHandlers(string eventType)
			{
				return false;
			}
		}
	}
}