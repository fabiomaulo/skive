﻿using System;
using System.Threading.Tasks;

namespace Hunabku.Skive
{
	public class ConsoleLogEventHandler: ISlackEventHandler
	{
		public Task Handle(ISlackEventContext context)
		{
			Console.WriteLine(context.EventContent);
			return Task.FromResult((object) null);
		}
	}
}