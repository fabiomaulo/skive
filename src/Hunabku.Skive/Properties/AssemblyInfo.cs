﻿using System.Reflection;

[assembly: AssemblyTitle("Hunabku.Skive")]
[assembly: AssemblyDescription("Skive is a lightweight Bot engine to create your own Slack Bot.")]
[assembly: AssemblyCompany("Hunabku")]
[assembly: AssemblyProduct("Hunabku.Skive")]
[assembly: AssemblyCopyright("Copyright ©  2016")]
[assembly: AssemblyCulture("")]

