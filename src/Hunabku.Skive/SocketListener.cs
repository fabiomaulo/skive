﻿using System;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Hunabku.Skive
{
	public class SocketListener : IDisposable
	{
		private const int receiveChunkSize = 1024;
		private readonly Uri uri;
		private CancellationToken cancellationToken;
		private CancellationTokenSource cancellationTokenSource;
		private bool disposed;
		private Action<string> onMessage;
		private ClientWebSocket ws;

		public SocketListener(string uri)
		{
			this.uri = new Uri(uri);
		}

		public void Dispose()
		{
			if (disposed)
			{
				return;
			}
			cancellationTokenSource.Cancel();
			cancellationTokenSource?.Dispose();
			ws?.Dispose();
			ws = null;
			disposed = true;
		}

		public async void Connect()
		{
			if (disposed)
			{
				throw new ObjectDisposedException("SocketListener");
			}
			cancellationTokenSource = new CancellationTokenSource();
			cancellationToken = cancellationTokenSource.Token;
			ws = new ClientWebSocket();
			ws.Options.KeepAliveInterval = TimeSpan.FromSeconds(20);
			await ws.ConnectAsync(uri, cancellationToken);
			StartListen();
		}

		public SocketListener OnMessage(Action<string> onMessageAction)
		{
			onMessage = onMessageAction;
			return this;
		}

		private async void StartListen()
		{
			var buffer = new byte[receiveChunkSize];

			try
			{
				while (ws.State == WebSocketState.Open && !cancellationToken.IsCancellationRequested)
				{
					var stringResult = new StringBuilder();
					WebSocketReceiveResult result;
					do
					{
						result = await ws.ReceiveAsync(new ArraySegment<byte>(buffer), cancellationToken);

						if (result.MessageType == WebSocketMessageType.Close)
						{
							await
								ws.CloseAsync(WebSocketCloseStatus.NormalClosure, string.Empty, CancellationToken.None);
						}
						else
						{
							var str = Encoding.UTF8.GetString(buffer, 0, result.Count);
							stringResult.Append(str);
						}
					} while (!result.EndOfMessage && !cancellationToken.IsCancellationRequested);
					ConsumeMessage(stringResult);
				}
			}
			catch (OperationCanceledException) {}
			finally
			{
				Dispose();
			}
		}

		private void ConsumeMessage(StringBuilder message)
		{
			if (onMessage != null && !cancellationToken.IsCancellationRequested)
			{
				Task.Run(() => onMessage(message.ToString()), cancellationToken);
			}
		}
	}
}