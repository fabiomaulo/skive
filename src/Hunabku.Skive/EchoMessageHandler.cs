﻿using System.Threading.Tasks;

namespace Hunabku.Skive
{
	public class EchoMessageHandler : ISlackEventHandler
	{
		public Task Handle(ISlackEventContext context)
		{
			dynamic eventData = context.Event;
			string subtype = eventData.subtype;
			var channelType = context.GetChannelType();
			string receivedText = eventData.text;
			if (!string.IsNullOrEmpty(subtype) || context.UserIsBot() ||
					(channelType != ChannelType.DirectMessage && !context.BotProfile.WasMentionedIn(receivedText)))
			{
				return Task.FromResult((object) null);
			}
			string channelId = eventData.channel;

			return context.BotChatPostMessage(channelId, receivedText);
		}
	}
}