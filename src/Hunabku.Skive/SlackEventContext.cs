﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace Hunabku.Skive
{
	internal class SlackEventContext : ISlackEventContext
	{
		private readonly ConcurrentDictionary<string, object> environment;

		public SlackEventContext()
		{
			environment = new ConcurrentDictionary<string, object>(StringComparer.Ordinal);
		}

		public string AuthToken { get; set; }
		public IBotProfile BotProfile { get; set; }
		public IDictionary<string, object> Environment => environment;
		public string EventContent { get; set; }
		public dynamic Event { get; set; }

		public void SetContext(IEnumerable<KeyValuePair<string, object>> sharedContext)
		{
			foreach (var keyValuePair in sharedContext)
			{
				environment[keyValuePair.Key] = keyValuePair.Value;
			}
		}

		public T Get<T>(string key)
		{
			object value;
			return Environment.TryGetValue(key, out value) ? (T) value : default(T);
		}

		public ISlackEventContext Set<T>(string key, T value)
		{
			environment[key] = value;
			return this;
		}
	}
}