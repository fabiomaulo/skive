﻿namespace Hunabku.Skive
{
	public interface IBotProfile
	{
		string Id { get; }
		string Name { get; }
		string Image48Url { get; }
		bool WasMentionedIn(string message);
	}
}