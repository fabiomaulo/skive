﻿using System.Collections.Generic;

namespace Hunabku.Skive
{
	/// <summary>
	///   Create the default environment objects at rtm.start (https://api.slack.com/methods/rtm.start).
	/// </summary>
	/// <remarks>
	///   The default environment will be merged in the <see cref="ISlackEventContext" /> for each event.
	/// </remarks>
	public interface IEnvironmentBuilder
	{
		/// <summary>
		///   Build the whole environment shared across all events.
		/// </summary>
		/// <param name="startEventContent">The content of the response of the slack API.</param>
		/// <param name="startEvent">A dynamic object representing the deserialized verion of the slack API response.</param>
		/// <returns>
		///   Environment information you want hold for each slack event (https://api.slack.com/events)
		/// </returns>
		/// <remarks>
		///   "BotProfile" is the defaut key to hold <see cref="IBotProfile" /> instance; you can override it with your custom
		///   instance.
		/// </remarks>
		IEnumerable<KeyValuePair<string, object>> BuildEnvironment(string startEventContent, dynamic startEvent);
	}
}