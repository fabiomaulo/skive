﻿using System.Text.RegularExpressions;

namespace Hunabku.Skive
{
	public class BotProfile : IBotProfile
	{
		private Regex mentioned;
		public string Id { get; set; }
		public string Name { get; set; }
		public string Image48Url { get; set; }

		public bool WasMentionedIn(string message)
		{
			return message != null && (mentioned ?? (mentioned = CreateMentionedRegex())).IsMatch(message);
		}

		private Regex CreateMentionedRegex()
		{
			return new Regex(string.Concat("(<@", Id, ">)"), RegexOptions.IgnoreCase | RegexOptions.Multiline);
		}
	}
}