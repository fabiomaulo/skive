﻿using System.Collections.Generic;

namespace Hunabku.Skive
{
	/// <summary>
	///   The context of a slack event shared in the whole event pipeline.
	/// </summary>
	public interface ISlackEventContext
	{
		/// <summary>
		///   Get the authentication token.
		/// </summary>
		string AuthToken { get; }

		/// <summary>
		///   The Bot profile.
		/// </summary>
		IBotProfile BotProfile { get; }

		/// <summary>
		///   The event environment.
		/// </summary>
		IDictionary<string, object> Environment { get; }

		/// <summary>
		///   Gets the original event content (JSON)
		/// </summary>
		string EventContent { get; }

		/// <summary>
		///   Gets the dynamic object representing event content.
		/// </summary>
		dynamic Event { get; }

		/// <summary>
		///   Gets a value from the event environment, or returns default(T) if not present.
		/// </summary>
		/// <typeparam name="T">The type of the value.</typeparam>
		/// <param name="key">The key of the value to get.</param>
		/// <returns>The value with the specified key or the default(T) if not present.</returns>
		T Get<T>(string key);

		/// <summary>
		///   Sets the given key and value in the OWIN environment.
		/// </summary>
		/// <typeparam name="T">The type of the value.</typeparam>
		/// <param name="key">The key of the value to set.</param>
		/// <param name="value">The value to set.</param>
		/// <returns>This instance.</returns>
		ISlackEventContext Set<T>(string key, T value);
	}
}