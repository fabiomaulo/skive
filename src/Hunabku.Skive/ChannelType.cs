namespace Hunabku.Skive
{
	public enum ChannelType
	{
		Undefined,
		Channel,
		DirectMessage,
		Group
	}
}