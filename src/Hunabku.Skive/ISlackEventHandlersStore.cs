﻿using System.Collections.Generic;

namespace Hunabku.Skive
{
	public interface ISlackEventHandlersStore
	{
		/// <summary>
		///   Get all registered events handlers.
		/// </summary>
		/// <param name="eventType">The name of the event-type defined in https://api.slack.com/events </param>
		/// <returns>Instances of ISlackEventHandler or an empty enumerable.</returns>
		IEnumerable<ISlackEventHandler> GetHandlersOf(string eventType);

		/// <summary>
		///   Determine if an event has registered hadlers.
		/// </summary>
		/// <param name="eventType">The name of the event-type defined in https://api.slack.com/events </param>
		/// <returns>true if the event has handlers</returns>
		bool HasHandlers(string eventType);
	}
}